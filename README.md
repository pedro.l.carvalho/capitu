# Capitu Python Module

## Introduction
Capitu is a python module programmed to capture and present metering data from supported electrical energy meters.

## Build with
 - Emacs
 - Python

## Development Status
Capitu is under construction.

## Needed Improvements
This code is being built as a proof of concept. It may need some standardization or optimization. You may feel free to contribute.

## Why Capitu?
[Capitu](https://en.wikipedia.org/wiki/Capitu) is the nickname of Maria Capitolina Santiago, main character of the 1899 Machado de Assis' book Dom Casmurro. [Joaquim Maria Machado de Assis](https://en.wikipedia.org/wiki/Machado_de_Assis) (June 21, 1839 - September 29, 1908), often known as Machado de Assis, was a pioneer Brazilian novelist, poet, playwright and short story writer. He is widely regarded as the greatest writer of Brazilian literature.

In Portuguese, the word "Capitu" sounds almost like "capture", the exact purpose of this module: capture metering data.

Capitu is one of the greatest women in Brazilian literature. This powerful and dreamy woman is the subject of many academic studies, books, television programs, etc. The character, for sure, would deserve a much better homage than I do here.

Her intense way of living and her faithful friendship with Escobar caused her husband Betinho, the Dom Casmurro, to be affected by the distrust of having been betrayed. The work of Machado de Assis is inconclusive about the supposed affair of Capitu, and for this reason, perhaps this has become the most important question in Brazilian literature: Has Capitu betrayed Bentinho?

Metering data capture and presentation software is used worldwide to combat fraud in the distribution of electricity. Operators focus on the acquired data searching for the mystery solution: are these data proof of a theft of energy? Capitu evokes the mystery of betrayal as the telemetric data evoke it as well.

"Dá-me uma comparação exata e poética para dizer o que foram aqueles olhos de Capitu. Não me acode imagem capaz de dizer, sem quebra da dignidade do estilo, o que eles foram e me fizeram. Olhos de ressaca? Vá, de ressaca" (Machado de Assis).

## Authors
 - Daniel Campos Pompermayer

## License
Copyright (C) 2018  Daniel Campos Pompermayer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.