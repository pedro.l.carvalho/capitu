import time
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from ..logger import log_message
from ..clicker import click_on_element


def generate_inverter_data(serial_number, from_date, to_date, driver,
                           history_page, log, timeout=180, repetitions=10):

    # Register the attempt into the log file.
    log_message(log, "Starting proccess for getting data.", serial_number,
                from_date, to_date)

    for i in range(0, repetitions):
        try:

            # Go to the history data page.
            driver.get(history_page)

            # Confirm that the usines are on the list.
            _ = WebDriverWait(driver, timeout).until(
                lambda x: x.find_elements(By.CLASS_NAME, "el-collapse-item"))

            # Try to clear date before and log the result.
            click_on_element(By.CLASS_NAME,
                             "el-input__icon.el-range__close-icon", driver,
                             "Clear button", timeout)
            log_message(log, "Cleaned the date field.", serial_number,
                        from_date, to_date)

            # Fill the dates.
            fill_dates(from_date, to_date, driver)
            log_message(log, "Filled up the date fields", serial_number,
                        from_date, to_date)

            # Search for an inverter.
            search_inverter(serial_number, driver, timeout)
            log_message(log, "Searching inverter", serial_number,
                        from_date, to_date)

            # Expand the dropdown object related to the inverter.
            click_on_element(By.CLASS_NAME, "el-collapse-item", driver,
                             "Dropdown object", timeout)
            log_message(log, "Dropdown object expanded.", serial_number,
                        from_date, to_date)

            # Click on the add button for adding the inverter into the query.
            click_on_element(By.ID, serial_number, driver, "Add button",
                             timeout)
            log_message(log, "Inverter added to the query.", serial_number,
                        from_date, to_date)

            # Confirm that the variables were added.
            start_time = time.time()
            while (time.time() - start_time) < timeout:
                variables = driver.find_elements(By.CLASS_NAME,
                                                 "el-checkbox__input")
                if len(variables) > 0:
                    break
            else:
                raise TimeoutException(
                    "It seems that variables were not added")

            # Check all the variables.
            click_on_element(By.CLASS_NAME, "his_btn_all.btn-nor", driver,
                             "Check all Button", timeout)
            log_message(log, "Asking to check all the variables",
                        serial_number, from_date, to_date)

            # Confirm whether they are checked.
            # OBS.: There are at least 37 checkboxes which are composed by 3
            # elements so, I wanna get at least 111 checked elements.
            checkboxes = driver.find_elements(By.CLASS_NAME,
                                              "el-checkbox__input.is-checked")
            if len(checkboxes) < 111:
                raise ValueError("I've got " + str(len(checkboxes))
                                 + " checked checkboxes")
            log_message(log, "All the variables has been checked",
                        serial_number, from_date, to_date)

            # The generation button has not a singular name, so I need to find
            # all the buttons which have the same class name and search for
            # the text within.
            buttons = driver.find_elements(By.CLASS_NAME, "btn-nor")
            gen_buttons = [btn for btn in buttons if (btn.text == "Gerar")
                           or (btn.text == "Generate Data")]

            # Check how many buttons I've found. If none, raise an error.
            # If I found more than one having the right text, click on the
            # first one and let's see what we're getting.
            if len(gen_buttons) == 0:
                raise ValueError("I've got no generate buttons")
            gen_buttons[0].click()
            log_message(log, "Asked to generate data", serial_number,
                        from_date, to_date)
            break

        except TimeoutError as e:
            if len(e.args) == 0:
                log_message(log, "Try no. " + str(i) + ": TimeoutError",
                            serial_number, from_date, to_date)
            else:
                log_message(log, "Try no. " + str(i) + ": " + e.args[0],
                            serial_number, from_date, to_date)
            if i >= repetitions:
                raise e

        except RuntimeError as e:
            if len(e.args) == 0:
                log_message(log, "Try no. " + str(i) + ": Runtime Error",
                            serial_number, from_date, to_date)
            else:
                log_message(log, "Try no. " + str(i) + ": " + e.args[0],
                            serial_number, from_date, to_date)
            if i >= repetitions:
                raise e

        except Exception as e:
            if len(e.args) == 0:
                log_message(log, "Try no. " + str(i) + ": Unknown Error",
                            serial_number, from_date, to_date)
            else:
                log_message(log, "Try no. " + str(i) + ": " + e.args[0],
                            serial_number, from_date, to_date)
            if i >= repetitions:
                raise e


def fill_dates(from_date, to_date, driver):
    """
    Fill from and to date fields.
    """

    # Search for the "from" date field.
    try:
        from_date_field = driver.find_element(By.CSS_SELECTOR,
                                              "[placeholder='De']")
    except NoSuchElementException:
        from_date_field = driver.find_element(By.CSS_SELECTOR,
                                              "[placeholder='From']")

    # Clear and fill the field.
    from_date_field.clear()
    from_date_field.send_keys(from_date)

    # Search for the "to" date field.
    try:
        to_date_field = driver.find_element(By.CSS_SELECTOR,
                                            "[placeholder='Para ']")
    except NoSuchElementException:
        to_date_field = driver.find_element(By.CSS_SELECTOR,
                                            "[placeholder='To']")

    # Clear and fill the field.
    to_date_field.clear()
    to_date_field.send_keys(to_date)
    to_date_field.send_keys(Keys.RETURN)


def search_inverter(serial_number, driver, timeout):
    """Search for an inverter."""

    search_field = driver.find_element(By.CLASS_NAME,
                                       "search_ipt_ps.search_ipt")
    search_field.clear()
    search_field.send_keys(serial_number)

    # Confirm that a single usine is on the list.
    start_time = time.time()
    while (time.time() - start_time) < timeout:
        dropdowns = driver.find_elements(By.CLASS_NAME, "el-collapse-item")
        if len(dropdowns) == 1:
            break
    else:
        if len(dropdowns) > 1:
            raise TimeoutError("I keep getting more than one inverter")
        else:
            raise TimeoutError("I couldn't find that inverter")
