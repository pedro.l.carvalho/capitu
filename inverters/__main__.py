from .logger import logger
from .login import login
from .get_access import get_access
from .get_inverters_sn import get_inverters_sn
from .get_params import get_params
from .get_data import get_data
from .convert_data import convert_data


def main():

    # Get parameters parsed from terminal.
    (key_path, access_path, homepage, logged_page, history_page, driver_path,
     inverters_file, export_path, portal_export_filename, export_filename,
     interval_duration, start_date, end_date, timeout, repetitions,
     logpath) = get_params()

    # Create a log file.
    log = logger(logpath)

    # Decrypt username and password.
    username, password = get_access(key_path, access_path)

    # Log into the inverters portal.
    driver = login(username, password, homepage, logged_page, log, driver_path,
                   export_path, timeout)

    # Get inverters directory.
    serial_numbers = get_inverters_sn(inverters_file, log, sn_null="drop")

    # Get data.
    get_data(log, driver, serial_numbers, start_date, end_date,
             interval_duration, history_page, export_path,
             portal_export_filename, timeout, repetitions)

    # Close the browser.
    # driver.close closes a single tab.
    driver.quit()

    # Convert and save data.
    convert_data(log, export_path, portal_export_filename, export_filename)


if __name__ == "__main__":
    main()
