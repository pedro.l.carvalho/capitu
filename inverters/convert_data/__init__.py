import pandas as pd
import datetime as dt
from pathlib import Path
from xlrd.biffh import XLRDError
from ..logger import log_message


def convert_data(log, export_path="/home/capitu/Downloads/",
                 portal_export_filename="Historical Data Export-*.xls",
                 export_filename="Historical Data Export"):

    try:

        # Register the convertion proccess into the log file.
        log_message(log, "Starting proccess for convert and save data.")

        # Instance a path object pointing to the exportation directory.
        path = Path(export_path)

        # Get the exportation files.
        ftr_files = [file for file in path.glob(export_filename + "-*.ftr")]

        # Create an empty data frame for merging all the exported data
        # together.
        exportation_df = pd.DataFrame()

        # Check whether there are some exportation files.
        # If we do have somes files, read and concat them all into the data
        # frame.
        if len(ftr_files) > 0:
            for file in ftr_files:
                exportation_df = pd.concat([
                    exportation_df, pd.read_feather(file)], ignore_index=True)

        log_message(log, "Opened the exportation files.")

        # Get the exported data files.
        data_files = [file for file in path.glob(portal_export_filename)]

        # Read the exported data and append it to the exportation data frame.
        for file in data_files:

            try:

                # Register the file to be read and converted.
                log_message(log, "Starting reading and converting: "
                            + file.name)

                # Read each individual file.
                data = pd.read_excel(file)

                # Get the serial number
                serial_number = data.iloc[0, 1]

                # Get the header row.
                header = data.iloc[1, :]

                # Keep only the useful data.
                data = data.iloc[2:, :]

                # Set columns names.
                data.columns = header

                # Create a column for serial number
                data["serial_number"] = serial_number

                # Append it to the exportation data frame.
                exportation_df = pd.concat([exportation_df, data],
                                           ignore_index=True)

                # Register the success on reading and converting file.
                log_message(log, file.name + " was read.")

            # Catch exception raised by the xls reader engine.
            except XLRDError as e:
                if len(e.args) == 0:
                    log_message(log, "Unknown Error")
                    data_files.remove(file)
                else:
                    if e.args[0][37:63] == "BOF not workbook/worksheet":
                        log_message(log, "Empty spreadsheet.")
                    else:
                        log_message(log, e.args[0])
                        data_files.remove(file)

            # Catch general exceptions so my code doesn't stop running.
            except Exception as e:
                if len(e.args) == 0:
                    log_message(log, "Unknown Error")
                else:
                    log_message(log, e.args[0])
                data_files.remove(file)

        # Remove duplicates it there are some.
        exportation_df = exportation_df.drop_duplicates(ignore_index=True)

        # Save exportation data frame.
        full_export_filename = path / (export_filename + "-"
                                       + dt.datetime.now().strftime("%d-%m-%Y")
                                       + ".ftr")
        exportation_df.to_feather(full_export_filename)

        # Read the just exported file to confirm all went well.
        exported_df = pd.read_feather(full_export_filename)

        # Check whether the read dataframe is equal to the saved one.
        if not exported_df.equals(exportation_df):
            log_message(log, "Read data frame is different from saved one.")

        # Check whether the new feather name has the name of some of the olds.
        if full_export_filename in ftr_files:
            ftr_files.remove(full_export_filename)

        log_message(log, "Data is converted and saved.")

        # Delete the data files and the old ftr files.
        for file in data_files:
            Path.unlink(file)
        for file in ftr_files:
            Path.unlink(file)

        log_message(log, "Cleaned directory.")

    except Exception as e:
        if len(e.args) == 0:
            log_message(log, "Unknown Error")
        else:
            log_message(log, e.args[0])
        raise e
